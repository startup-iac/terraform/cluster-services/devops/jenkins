# For more info view:
# https://artifacthub.io/packages/helm/jenkinsci/jenkins

locals {
  secret_name = "${var.dns_name}-tls"
  values_file = "values-${var.chart_version}.yaml"
}

data "template_file" "values" {
  template = "${file("${path.module}/${local.values_file}")}"
  vars = {
    DNS_NAME          = var.dns_name
    IAM_DNS_NAME      = var.iam_dns_name
    IAM_CLIENT_SECRET = var.iam_client_secret
  }
}


resource "helm_release" "this" {
  name             = "jenkins"
  namespace        = var.namespace
  version          = var.chart_version

  repository = "https://charts.jenkins.io"
  chart      = "jenkins"

  values = [
    data.template_file.values.rendered
  ]

  set {
    name  = "controller.ingress.hostName"
    value = var.dns_name
  }

  set {
    name  = "controller.ingress.tls[0].secretName"
    value = local.secret_name
  }

  set {
    name  = "controller.ingress.tls[0].hosts[0]"
    value = var.dns_name
  }

  # Ingress Annotations
  set {
    name  = "controller.ingress.annotations.cert-manager\\.io/cluster-issuer"
    value = var.cluster_issuer_name
  }  

  set {
    name  = "controller.ingress.annotations.cert-manager\\.io/common-name"
    value = var.dns_name
  }  

  set {
    name  = "controller.ingress.annotations.cert-manager\\.io/subject-organizations"
    value = var.subject_organizations
  }  

  set {
    name  = "controller.ingress.annotations.cert-manager\\.io/subject-organizationalunits"
    value = var.subject_organizationalunits
  }  


}

