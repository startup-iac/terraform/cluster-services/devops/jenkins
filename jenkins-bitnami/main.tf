# For more info view:
# https://artifacthub.io/packages/helm/bitnami/jenkins


resource "helm_release" "this" {
  name             = "jenkins"
  namespace        = var.namespace

  repository = "https://charts.bitnami.com/bitnami"
  chart      = "jenkins"

  values = [
    "${file("${path.module}/values.yaml")}"             
  ]

  set {
     name  = "ingress.hostname"
     value = var.dns_name
  }

  # Ingress Annotations
  set {
    name  = "ingress.annotations.cert-manager\\.io/cluster-issuer"
    value = var.cluster_issuer_name
  }  

  set {
    name  = "ingress.annotations.cert-manager\\.io/common-name"
    value = var.dns_name
  }  

  set {
    name  = "ingress.annotations.cert-manager\\.io/subject-organizations"
    value = var.subject_organizations
  }  

  set {
    name  = "ingress.annotations.cert-manager\\.io/subject-organizationalunits"
    value = var.subject_organizationalunits
  }

}

