
module "jenkins" {
  source = "./jenkins"

  namespace                   = var.namespace
  chart_version               = var.chart_version
  dns_name                    = var.dns_name
  cluster_issuer_name         = var.cluster_issuer_name
  subject_organizations       = var.subject_organizations
  subject_organizationalunits = var.subject_organizationalunits
  iam_dns_name                = var.iam_dns_name
  iam_client_secret           = var.iam_client_secret

}

